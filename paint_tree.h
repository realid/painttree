#ifndef PAINTTREE_H
#define PAINTTREE_H

#include <QWidget>
#include "avl_tree.h"

class QPaintEvent;
class QPainter;

class PaintTree : public QWidget
{
    public:
        PaintTree();
        void paintEvent(QPaintEvent *event);
    public:
        AvlTree<int>* pTree;
};
#endif
