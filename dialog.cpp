#include <QtGui>
#include "dialog.h"

MyDialog::MyDialog(QWidget *parent): QDialog(parent)
{
    setupUi(this);
    connect(Insert, SIGNAL(clicked()), this, SLOT(InsertClicked()));
    connect(Exit, SIGNAL(clicked()), qApp, SLOT(quit()));
    tree.show();
}

void MyDialog::InsertClicked()
{
    int value = InsertBox->value();
    tree.pTree->Insert(value);
    tree.update();
    return;
}
