#ifndef DIALOG_H
#define DIALOG_H

#include "ui_paint_tree.h"
#include "paint_tree.h"

class MyDialog: public QDialog, public Ui_Dialog
{
    Q_OBJECT
    public:
        MyDialog(QWidget *parent = 0);
    private slots:
        void InsertClicked();
    private:
        PaintTree tree;
};

#endif
